<?php
/**
 * Parcel Locker Module
 *
 * @package   gmparcellocker
 * @author    Dariusz Tryba (contact@greenmousestudio.com)
 * @copyright Copyright (c) Green Mouse Studio (http://www.greenmousestudio.com)
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
if (!defined('_PS_VERSION_')) {
    exit;
}

class GmParcelLocker extends Module
{

    public function __construct()
    {
        $this->name = 'gmparcellocker';
        $this->prefix = strtoupper($this->name);
        $this->tab = 'shipping_logistics';
        $this->version = '1.0.0';
        $this->author = 'GreenMouseStudio.com';
        $this->need_instance = 0;
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Parcel locker');
        $this->description = $this->l('Allows the customer to chose a parcel locker in the cart');

        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
    }

    public function install()
    {
        if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        }
        if (!parent::install() || !$this->registerHook('displayInfoByCart') || !$this->registerHook('displayCarrierExtraContent')
            || !$this->installDb() || !$this->createCarriers() || !$this->registerHook('header')
        ) {
            return false;
        }
        return true;
    }

    public function getContent()
    {
        $content = '';
        $content .= '<p class="alert alert-warning">'.$this->l('Remember to configure and activate the new Parcel Locker carrier created by the module').'</p>';
        return $content.$this->context->smarty->fetch($this->local_path.'views/templates/admin/gms.tpl');
    }

    public function displayInfoByCart($cartId)
    {
        $pointData = $this->getPointDataForCart($cartId);
        $this->context->smarty->assign('gmChosenParcel', $pointData);
        return $this->context->smarty->fetch($this->local_path.'views/templates/admin/parcel.tpl');
    }

    protected function createCarriers()
    {
        $carrier = new Carrier();
        $carrier->name = $this->l('Parcel Locker');
        $carrier->active = 0;
        $carrier->is_free = 0;
        $carrier->shipping_handling = 1;
        $carrier->shipping_external = 0;
        $carrier->shipping_method = 1;
        $carrier->max_width = 0;
        $carrier->max_height = 0;
        $carrier->max_depth = 0;
        $carrier->max_weight = 0;
        $carrier->grade = 0;
        $carrier->is_module = 1;
        $carrier->need_range = 1;
        $carrier->range_behavior = 1;
        $carrier->external_module_name = $this->name;
        $carrier->url = 'https://inpost.pl/sledzenie-przesylek?number=@';

        $delay = array();

        foreach (Language::getLanguages(false) as $language) {
            $delay[$language['id_lang']] = $this->l('Parcel Locker');
        }

        $carrier->delay = $delay;
        if (!$carrier->save()) {
            return false;
        }
        $groups = [];
        foreach (Group::getGroups((int) Context::getContext()->language->id) as $group) {
            $groups[] = $group['id_group'];
        }
        if (!$carrier->setGroups($groups)) {
            return false;
        }

        return true;
    }

    protected function installDb()
    {
        return Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'gmparcellocker` (
			`id_cart` int(10) unsigned NOT NULL,
            `parcel` varchar(128),
            PRIMARY KEY (`id_cart`)
		) ENGINE='._MYSQL_ENGINE_.' default CHARSET=utf8');
    }

    public function uninstall()
    {
        Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'gmparcellocker`');
        return parent::uninstall();
    }

    public function hookDisplayCarrierExtraContent($params)
    {
        $cartId = $this->context->cart->id;
        $pointData = $this->getPointDataForCart($cartId);
        $this->context->smarty->assign('gmChosenParcel', $pointData);
        return $this->context->smarty->fetch($this->local_path.'views/templates/button.tpl');
    }

    public function getPointDataForCart($cartId)
    {
        $pointData = Db::getInstance()->getValue('SELECT `parcel` FROM `'._DB_PREFIX_.'gmparcellocker` WHERE `id_cart` = '.$cartId);
        if ($pointData) {
            return $pointData;
        }
        return '---';
    }

    public function hookHeader()
    {
        if ($this->context->controller->php_self === 'order') {
            $cartId = Context::getContext()->cart->id;
            Media::addJsDef(['gmCartId' => $cartId,
                'gmParcelLockerAjaxUrl' => $this->context->link->getModuleLink('gmparcellocker', 'ajax', array(), null,
                    null, null, true),]);
            $this->context->controller->registerStylesheet('gmparcellocker-css',
                'modules/'.$this->name.'/views/css/gmparcellocker.css',
                [
                    'media' => 'all',
                    'priority' => 200,
            ]);

            $this->context->controller->registerJavascript('gmparcellocker-js',
                'modules/'.$this->name.'/views/js/gmparcellocker.js',
                [
                    'position' => 'bottom',
                    'priority' => 150
            ]);
        }
        return '<script async src="https://geowidget.easypack24.net/js/sdk-for-javascript.js"></script>
                <link rel="stylesheet" href="https://geowidget.easypack24.net/css/easypack.css"/>';
    }
}
