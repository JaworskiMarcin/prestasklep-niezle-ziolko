<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{gmparcellocker}prestashop>gmparcellocker_c5824aabdfcfe98d5950818bab0260e6'] = 'Paczkomaty';
$_MODULE['<{gmparcellocker}prestashop>gmparcellocker_81f5ab314898589259232be18d0c96de'] = 'Umożliwia wybór paczkomatu w koszyku';
$_MODULE['<{gmparcellocker}prestashop>gmparcellocker_24d11524546e70303f21a814b7b2388d'] = 'Pamiętaj, aby skonfigurować i aktywować nowego przewoźnika Paczkomaty, utworzonego przez ten moduł';
$_MODULE['<{gmparcellocker}prestashop>gmparcellocker_39b6fda48dbec22f92c82517456047c2'] = 'Paczkomaty';
$_MODULE['<{gmparcellocker}prestashop>button_04f689478e79ad944c1893dc7e713810'] = 'Wybrany paczkomat:';
$_MODULE['<{gmparcellocker}prestashop>button_494bd131a34e4dc02be773266700e935'] = 'Wybierz paczkomat';
