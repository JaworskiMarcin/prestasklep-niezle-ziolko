<div class="gmparcellocker-button col-sm-12">
   <p>{l s='Chosen Parcel Locker:' mod='gmparcellocker'} <span class="chosen-parcel">{$gmChosenParcel}</span></p>
   <button class="btn btn-default btn-primary" id="parcel-choose">
      {l s='Choose Parcel Locker' mod='gmparcellocker'}
   </button>
</div>