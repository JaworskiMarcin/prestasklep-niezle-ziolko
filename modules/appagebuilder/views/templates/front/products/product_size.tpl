{* 
* @Module Name: AP Page Builder
* @Website: apollotheme.com - prestashop template provider
* @author Apollotheme <apollotheme@gmail.com>
* @copyright Apollotheme
* @description: ApPageBuilder is module help you can build content for your shop
*}
<!-- @file modules\appagebuilder\views\templates\front\products\file_tpl -->

<ul class="product_attr" style="display: inline-flex;">
    {foreach from=$product.attribute.Size item=size}
        <li class="product_{$size.public_name}" style="margin:0 7.5px;">
            <a class="{$size.public_name}" title="{$size.name}" href="{$size.url}">{$size.name}</a>
        </li>
    {/foreach}
</ul>
