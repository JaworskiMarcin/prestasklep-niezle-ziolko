{* 
* @Module Name: AP Page Builder
* @Website: apollotheme.com - prestashop template provider
* @author Apollotheme <apollotheme@gmail.com>
* @copyright Apollotheme
* @description: ApPageBuilder is module help you can build content for your shop
*}
<!-- @file modules\appagebuilder\views\templates\front\products\file_tpl -->

{foreach from=$product.attribute item=attribute key=key}
	{if $attribute}
	<div class="product_full_attr">
		{* <div style="float: left;"><span>{$key|replace:'_':' '}</span></div> *}
	    <ul class="product_attr" style="display: inline-flex;">
	    {foreach from=$attribute item=attr}
	    	{if $attr.group_type == 'color'}
		        <li class="color product_{$attr.public_name}" style="margin:0 7.5px;background-color: {$attr.color};">
		            <a class="{$attr.public_name}" title="{$attr.name}" href="{$attr.url}"></a>
		        </li>
		    {else}
		    	<li class="product_{$attr.public_name}" style="margin:0 7.5px;">
		            <a class="{$attr.public_name}" title="{$attr.name}" href="{$attr.url}">{$attr.name}</a>
		        </li>
	        {/if}
	    {/foreach}
	    </ul>
	</div>
	{/if}
{/foreach}